import 'package:flutter/material.dart';

class Authenticator extends StatefulWidget {
  late final ValueChanged<bool> onAuthenticated;

  Authenticator({ required this.onAuthenticated });

  @override
  _AuthenticatorState createState() => new _AuthenticatorState(onAuthenticated: onAuthenticated);
}

class _AuthenticatorState extends State<Authenticator> {
  late TextEditingController _user;
  late TextEditingController _pass;
  late final ValueChanged<bool> onAuthenticated;

  _AuthenticatorState({ required this.onAuthenticated });

  @override
  void initState() {
    super.initState();
    _user = new TextEditingController();
    _pass = new TextEditingController();
  }

  void _onPressed() {
    if (_user.text != 'user' || _pass.text != '1234') {
      onAuthenticated(false);
    } else {
      onAuthenticated(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new Padding(
        padding: new EdgeInsets.all(15.0),
        child: new Column(
          children: <Widget>[
            new TextField(
              controller: _user,
              decoration: new InputDecoration(labelText: 'Username'),
            ),
            new TextField(
              controller: _pass,
              decoration: new InputDecoration(labelText: 'Password'),
              obscureText: true,
            ),
            new Padding(
              padding: new EdgeInsets.all(10.0),
              child: new ElevatedButton(child: new Text('Login'), onPressed: _onPressed,)
            )
          ],
        ),
      ),
    );
  }
}