import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: new MyApp(),
  ));
}

enum Animals { Cat, Dog, Bird, Lizard, Fish }

class MyApp extends StatefulWidget {
  @override
  _State createState() => new _State();
}

class _State extends State<MyApp> {
  
  Animals _selected = Animals.Cat;
  String _value = "Make a selection";
  List<PopupMenuEntry<Animals>> _items = new List<PopupMenuEntry<Animals>>();

  @override
  void initState() {
    super.initState();

    for (Animals animal in Animals.values) {
      _items.add(new PopupMenuItem<Animals>(
        child: new Text(_getDisplay(animal)),
        value: animal,
      ));
    }
  }

  void _onSelected(Animals animal) {
    setState(() {
      _selected = animal;
      _value = "You selected ${_getDisplay(animal)}";
    });
  }

  String _getDisplay(Animals animal) {
    int index = animal.toString().indexOf('.');
    index++;
    return animal.toString().substring(index);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("MyApp")
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Container(
                padding: new EdgeInsets.all(5.0),
                child: new Text(_value),
              ),
              new PopupMenuButton<Animals>(
                child: new Icon(Icons.input),
                onSelected: _onSelected,
                itemBuilder: (BuildContext context) {
                  return _items;
                }
              )
            ],
          ),
        )
      ),
    );
  }
}