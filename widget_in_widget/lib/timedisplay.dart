import 'package:flutter/material.dart';

class TimeDisplay extends StatelessWidget {

  late final Duration duration;
  late final Color color;
  late final ValueChanged<Duration> onClear;

  TimeDisplay({ required this.color, required this.duration, required this.onClear });

  void _onClicked() {
    onClear(duration);
  }

  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Padding(
          padding: new EdgeInsets.all(5.0),
          child: new Text(duration.toString(), style: new TextStyle(fontSize: 32.0, color: color)),
        ),
        new IconButton(icon: new Icon(Icons.clear), onPressed: _onClicked)
      ],
    );
  }

}