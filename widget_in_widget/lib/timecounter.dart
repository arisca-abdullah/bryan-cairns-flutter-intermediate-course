import 'dart:async';
import 'package:flutter/material.dart';
import 'package:widget_in_widget/timedisplay.dart';

class TimeCounter extends StatefulWidget {
  @override
  _TimeCounterState createState() => new _TimeCounterState();
}

class _TimeCounterState extends State<TimeCounter> {
  late Stopwatch _watch;
  late Timer _timer;
  late Duration _duration;

  void _onStart() {
    setState(() {
      _watch = new Stopwatch();
      _timer = new Timer.periodic(new Duration(milliseconds: 1), _onTimeout);
    });

    _watch.start();
  }

  void _onStop() {
    _timer.cancel();
    _watch.stop();
  }

  void _onTimeout(Timer timer) {
    if (!_watch.isRunning) return;

    setState(() => _duration = _watch.elapsed);
  }

  void _onClear(Duration value) {
    setState(() => _duration = new Duration());
  }

  @override
  void initState() {
    super.initState();
    _duration = new Duration();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(10.0),
      child: new Center(
        child: new Column(
          children: <Widget>[
            new TimeDisplay(
              color: Colors.red,
              duration: _duration,
              onClear: _onClear
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(10.0),
                  child: new ElevatedButton(child: new Text('Start'), onPressed: _onStart,),
                ),
                new Padding(
                  padding: new EdgeInsets.all(10.0),
                  child: new ElevatedButton(child: new Text('Stop'), onPressed: _onStop,),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}