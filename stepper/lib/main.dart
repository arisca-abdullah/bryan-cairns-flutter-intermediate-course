import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _State createState() => new _State();
}

class _State extends State<MyApp> {
  List<Step> _steps;
  int _current;

  @override
  void initState() {
    super.initState();

    _current = 0;
    _steps = <Step>[
      new Step(title: new Text("Step 1"), content: new Text('Do Something'), isActive: true),
      new Step(title: new Text("Step 2"), content: new Text('Do Something'), isActive: true),
      new Step(title: new Text("Step 3"), content: new Text('Do Something'), isActive: true),
    ];
  }

  void _stepNext() {
    setState(() {
      _current++;
      if (_current >= _steps.length) {
        _current = _steps.length - 1;
      }
    });
  }

  void _stepPrev() {
    setState(() {
      _current--;
      if (_current < 0) {
        _current = 0;
      }
    });
  }

  void _stepTap(int index) {
    setState(() {
      _current = index;
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("MyApp")
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Stepper(
            steps: _steps,
            type: StepperType.vertical,
            currentStep: _current,
            onStepContinue: _stepNext,
            onStepCancel: _stepPrev,
            onStepTapped: _stepTap,
          )
        )
      ),
    );
  }
}