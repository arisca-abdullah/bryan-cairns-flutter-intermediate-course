import 'package:flutter/material.dart';
import 'dart:math';
import 'package:charts_flutter/flutter.dart' as charts;

void main() {
  runApp(MaterialApp(
    home: new MyApp(),
  ));
}

class Sales {
  String year;
  int sales;

  Sales(this.year, this.sales);
}

class MyApp extends StatefulWidget {
  @override
  _State createState() => new _State();
}

class _State extends State<MyApp> {

  List<Sales> _laptops;
  List<Sales> _desktops;
  List<charts.Series<Sales, String>> _chartData;

  @override
  void initState() {
    super.initState();
    _makeData();
  }

  void _makeData() {
    _laptops = new List<Sales>();
    _desktops = new List<Sales>();
    _chartData = new List<charts.Series<Sales, String>>();

    Random random = new Random();
    for (int i = 2017; i < 2021; i++) {
      _laptops.add(new Sales(i.toString(), random.nextInt(1000)));
      _desktops.add(new Sales(i.toString(), random.nextInt(1000)));
    }

    _chartData.add(new charts.Series(
      id: 'Sales',
      colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault, 
      data: _laptops,
      domainFn: (Sales sales, _) => sales.year,
      measureFn: (Sales sales, _) => sales.sales,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      displayName: 'Sales'
    ));

    _chartData.add(new charts.Series(
      id: 'Sales',
      colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault, 
      data: _desktops,
      domainFn: (Sales sales, _) => sales.year,
      measureFn: (Sales sales, _) => sales.sales,
      fillPatternFn: (_, __) => charts.FillPatternType.solid,
      displayName: 'Sales'
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("MyApp")
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Text("Sales data"),
              new Expanded(child: new charts.BarChart(_chartData))
            ],
          ),
        )
      ),
    );
  }
}