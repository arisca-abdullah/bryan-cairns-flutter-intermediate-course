import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Clock extends StatefulWidget {
  @override
  _ClockState createState() => new _ClockState();
}

class _ClockState extends State<Clock> {
  String _value = 'Time here';

  @override
  void initState() {
    super.initState();
    new Timer.periodic(new Duration(seconds: 1), _onTimer);
  }

  void _onTimer(Timer timer) {
    var now = new DateTime.now();
    var formatter = new DateFormat('hh:mm:ss');
    String formatted = formatter.format(now);
    setState(() => _value = formatted);
  }

  @override
  Widget build(BuildContext context) {
    return new Text(_value, style: new TextStyle(fontSize: 32.0));
  }
}