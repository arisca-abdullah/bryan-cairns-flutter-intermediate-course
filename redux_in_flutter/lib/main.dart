import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

enum Actions { Increment, Decrement }

int reducer(int state, dynamic action) {
  if (action == Actions.Increment) state++;
  if (action == Actions.Decrement) state--;
  return state;
}

void main() {
  final store = new Store(reducer, initialState: 0);

  runApp(new MyApp(store));
}

class MyApp extends StatelessWidget {
  final Store<int> store;

  MyApp(this.store);

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: new MaterialApp(
        title: 'Flutter Redux',
        home: new Scaffold(
          appBar: new AppBar(
            title: new Text('Flutter Redux')
          ),
          body: new Container(
            padding: new EdgeInsets.all(32.0),
            child: new Column(
              children: <Widget>[
                new StoreConnector<int, String>(
                  builder: (context, count) {
                    return new Text(count, style: new TextStyle(fontSize: 24.0),);
                  },
                  converter: (store) => store.state.toString()
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new IconButton(icon: new Icon(Icons.add), onPressed: () => store.dispatch(Actions.Increment)),
                    new IconButton(icon: new Icon(Icons.remove), onPressed: () => store.dispatch(Actions.Decrement))
                  ],
                )
              ],
            ),
          ),
        ),
      )
    );
  }
}
