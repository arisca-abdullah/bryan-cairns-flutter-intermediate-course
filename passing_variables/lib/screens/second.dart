import 'package:flutter/material.dart';

class Second extends StatefulWidget {
  final String name;

  Second(this.name);

  @override
  _SecondState createState() => new _SecondState(name);
}

class _SecondState extends State<Second> {
  String name;

  _SecondState(this.name);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Title here'),
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Text('Hello, $name!'),
              new ElevatedButton(onPressed: () => Navigator.of(context).pop(), child: new Text('Back'))
            ],
          ),
        ),
      ),
    );
  }
}