import 'package:flutter/material.dart';
import './screens/home.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Navigation',
      routes: <String, WidgetBuilder>{
        // All avaliable pages
        '/Home': (BuildContext context) => new Home()
      },
      home: new Home(), // first page displayed
    );
  }
}